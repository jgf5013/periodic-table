# Front-End
## Running the Project
* `> npm start`

#### Notes:
* This assumes you have already completed the steps in the [Prerequisites](#prerequisites) section
* This documentation uses `npm` but you may follow similar steps with `yarn`
* If this results in the app launching successfully but being hosted on a machine-specific name, preventing the browser from launching, run `> unsetenv HOST && npm start`


# Prerequisites 
1. Make sure NodeJS and NPM are installed
1. `> npm install`

# Testing
* `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)


# Back-End
"amplify push" will build all your local backend resources and provision it in the cloud
"amplify publish" will build all your local backend and frontend resources (if you have hosting category added) and provision it in the cloud

GraphQL endpoint: https://kauyiakgkfgjllw6tpc2ezhowe.appsync-api.us-east-2.amazonaws.com/graphql
GraphQL API KEY: da2-iblrw6qfofb6nj4gcul3rdtize

GraphQL endpoint: https://cd4nmoo7qfeizhrioootkqapma.appsync-api.us-east-2.amazonaws.com/graphql
GraphQL API KEY: da2-rata6ehztjfgnlwdov2saubw5u

GraphQL endpoint: https://vsumdsrzxfa7bjypx5xlcojbsi.appsync-api.us-east-2.amazonaws.com/graphql
GraphQL API KEY: da2-nnspyv3d4za7dnnezexptyeuoq