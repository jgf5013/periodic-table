import React, { createContext } from 'react';
import { render, screen } from '@testing-library/react';
import QuizItem from './QuizItem';
import { AppContext, IAppState } from '../../app-context';
import App from '../../App';
import { getRandomElement, customRender } from '../testing-utils';
import mockData from '../periodic-table/periodic-table.mock-data.json';

let realUseContext;
let useContextMock;
beforeEach(() => {
  realUseContext = React.useContext;
  useContextMock = React.useContext = jest.fn();
});
// Cleanup mock
afterEach(() => {
  React.useContext = realUseContext;
});

describe('QuizItem Periodic Table Element', () => {
  const element = getRandomElement(mockData.data.listElements.items);
  const state = {
    quiz: {
      atomicElement: element,
      promptCategory: 'name',
      currentIncorrectPile: [],
      aggregateIncorrectPile: []
    },
    atomicElements: []
  };
  const dispatch = () => null;
  const providerProps = { value: { state, dispatch }};
  test('should render an div with an element name inside of it', () => {
    useContextMock.mockReturnValue({state});
    const quizItem = customRender(<QuizItem />, { providerProps })
    expect(quizItem.getByText(element.name)).toBeInTheDocument();
  });
});