import React, { FC } from "react";
import { AppContext } from '../../app-context';
import { Element } from '../../models';


function QuizItem () {
    const context = React.useContext(AppContext);
    const { state } = context;
    const { quiz } = state;

    const prompt = quiz.atomicElement ? quiz.atomicElement[quiz.promptCategory as keyof Element] : null;
    return (
        <div>
            <p>{prompt}</p>
        </div>
    );
};

export default QuizItem;