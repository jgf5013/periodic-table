
import { AmplifySignOut } from '@aws-amplify/ui-react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import './NavBar.scss';

function NavBar() {
    return (
        <AppBar className="appBar" position="static">
            <Toolbar className="toolbar">
                <AmplifySignOut />
            </Toolbar>
        </AppBar>
    )
}

export default NavBar;