import { useContext } from 'react';
import { AppContext } from '../../app-context';
import TableElement, { AtomicElementProps } from '../atomic-element/AtomicElement';
import { elementListToElementMatrix } from './periodic-table.service';
import './PeriodicTable.scss';



const PeriodicTable = () => {
    const { state } = useContext(AppContext);
    const { atomicElements } = state;
    const elementsMatrix = atomicElements ? elementListToElementMatrix(atomicElements) : [];
    const tableOfElements = elementsMatrix.map((atomicElementProps: (AtomicElementProps|undefined), index: number) => {
        return (<TableElement key={"table-element-" + index} atomicElement={atomicElementProps?.atomicElement} />);
    });
    return (
        <div className="periodic-table">
            {tableOfElements}
        </div>
    );
}



export default PeriodicTable;