import { RenderResult } from '@testing-library/react';
import React from 'react';
import { Element } from '../../models';
import { customRender } from '../testing-utils';
import mockData from './periodic-table.mock-data.json';
import PeriodicTable from './PeriodicTable';


let realUseContext;
let useContextMock;
beforeEach(() => {
  realUseContext = React.useContext;
  useContextMock = React.useContext = jest.fn();
});
// Cleanup mock
afterEach(() => {
  React.useContext = realUseContext;
});



describe('Layout', () => {
    const state = {
        atomicElements: [...mockData.data.listElements.items],
        quiz: {
            currentIncorrectPile: []
        }
    };
    // console.log('mockData =', mockData.data.listElements.items[0]);
    const dispatch = () => null;
    const providerProps = { value: { state, dispatch }};
    test('should display all elements passed in', () => {
        useContextMock.mockReturnValue({ state });
        const periodicTable: RenderResult = customRender(<PeriodicTable />, { providerProps })
        state.atomicElements.forEach((element: Element) => {
            const matcher = new RegExp(`^${element.symbol}$`);
            const e = periodicTable.getByText(matcher);
            expect(e).toBeInTheDocument();
        });
    });
});