import { API, graphqlOperation } from 'aws-amplify';
import { TABLE_COLUMNS, TABLE_ROWS } from '../../app.constants';
import { Element } from '../../models';
import { AtomicElementProps } from '../atomic-element/AtomicElement';
import * as elementsJson from '../../elements.json';

import { listElements } from '../../graphql/queries';
import { createElement as mutationCreateElement, deleteElement as mutationDeleteElement } from '../../graphql/mutations';

export const elementListToElementMatrix = (elements: Element[]): (AtomicElementProps | undefined)[] => {
    const table: (AtomicElementProps | undefined)[] = new Array(TABLE_COLUMNS * TABLE_ROWS).fill(undefined);
    elements.forEach((atomicElement) => {
        table[(TABLE_COLUMNS * ((atomicElement.ypos ?? 0) - 1)) + (atomicElement.xpos ?? 0) - 1] = {
            atomicElement
        };
    });
    return table;
};


export const addElement = async (tableElement: any): Promise<any> => {
  const response = await API.graphql(graphqlOperation(mutationCreateElement, { input: tableElement }));
  return response
}

export const deleteAllElement = async (elements: any[]) => {
  elements.forEach(async (e: any) => {
    await API.graphql(graphqlOperation(mutationDeleteElement, { input: {id: e.id, _version: e._version } }));
  });
}

export const fetchElements = async (): Promise<any[]> => {
    const variables = { limit: 1000 };
    const apiData: any = await API.graphql(graphqlOperation(listElements, variables));
    const nonDeletedElements = apiData.data.listElements.items.filter((e: any) => !e._deleted);
    return nonDeletedElements;
}


function isAlreadySaved(savedElements: any[], e: any): boolean {
  return savedElements.map(se => se.name).indexOf(e.name) !== -1;
}


export const fetchUnresolvedElements = (resolvedElements: any[]) => {
  // only get the elements which we haven't yet save (loop through all elements and check if it's in saved)
  elementsJson.elements.filter(elementToCheck => !isAlreadySaved(resolvedElements, elementToCheck))
  .forEach((newElement) => {
      addElement(newElement);
  });
}