import React, { useEffect, useReducer } from 'react';
import { appReducer } from './app-reducer';
import { Element } from './models';

import { fetchElements } from './components/periodic-table/periodic-table.service';

export interface IQuizState {
    remainingQuestions?: Element[];
    atomicElement?: Element;
    promptCategory: string;
    currentIncorrectPile: number[],
    aggregateIncorrectPile: number[]
}

export interface IAppState {
    quiz: IQuizState,
    atomicElements?: Element[]
}

export enum AppActionType {
    AnswerQuestion = '[Periodic Table] Answer Question',
    TableElementsLoaded = '[PeriodicTable] Elements Loaded'
}

export interface IAppAction {
    type: AppActionType,
    payload?: any
}

const initialStateQuizItem: IQuizState = {
    promptCategory: 'name',
    currentIncorrectPile: [],
    aggregateIncorrectPile: []
};

export const initialStateApp: IAppState = {
    atomicElements: [],
    quiz: initialStateQuizItem
};

/* Note: The defaultValue argument is only used when a component does not have a matching Provider above it in the
 * tree. This can be helpful for testing components in isolation without wrapping them. Note: passing undefined as a
 * Provider value does not cause consuming components to use defaultValue. */
export const AppContext = React.createContext<{state: IAppState, dispatch: any}>({
    state: initialStateApp,
    dispatch: () => null
});


export const AppContextProvider = ({ children }: any) => {
    const [state, dispatch] = useReducer<React.Reducer<IAppState, IAppAction>>(appReducer, initialStateApp);
    useEffect(() => {
        if (!state.atomicElements?.length) {
            fetchElements()
                .then((elements: any) => {
                    dispatch({type: AppActionType.TableElementsLoaded, payload: elements});
            });
        }
    }, [state.atomicElements]);

    return (
        <AppContext.Provider
            value={{state, dispatch}}>
            {children}
        </AppContext.Provider>);
};



export function useCustomContext() {
  return React.useContext(AppContext);
}
