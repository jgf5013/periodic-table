import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Amplify from 'aws-amplify'; // generated on `amplify-push` command
import config from './aws-exports';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';

Amplify.configure({...config});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorkerRegistration.register();

reportWebVitals();