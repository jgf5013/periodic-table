import { AppActionType, IAppAction, IAppState, IQuizState } from "./app-context";

export const appReducer: React.Reducer<IAppState, IAppAction> = (state, action) => {
    switch (action.type) {
        case AppActionType.TableElementsLoaded:
            const elements = action.payload;
            const initialQuestionIndex = Math.floor(Math.random() * elements.length);
            const initialQuizQuestion = elements.splice(initialQuestionIndex, 1)[0];
            return {
                ...state,
                atomicElements: action.payload,
                quiz: {
                    ...state.quiz,
                    remainingQuestions: [...elements],
                    atomicElement: initialQuizQuestion
                }
            };
        case AppActionType.AnswerQuestion:
            return isAnswerCorrect(state.quiz, action.payload) ? 
                {...state, quiz: getNextQuestion(state.quiz)} :
                { ...state, quiz: markIncorrect(state.quiz, action.payload) };
      default:
          throw new Error(`Unrecognized action. action.type=${action.type}`);
    }
}

const isAnswerCorrect = (quizItem: IQuizState, atomicNumber: number) => {
    return quizItem.atomicElement?.number === atomicNumber;
};

const getNextQuestion = (quizItem: IQuizState): IQuizState => {
    if (!quizItem.remainingQuestions) {
        throw new Error('Attempt to get the next quiz question before questions have been loaded!! :(');
    }
    const initialQuestionIndex = Math.floor(Math.random() * quizItem.remainingQuestions.length);
    const nextQuestion = quizItem.remainingQuestions.splice(initialQuestionIndex, 1)[0];

    return {
        ...quizItem,
        atomicElement: nextQuestion,
        remainingQuestions: [...quizItem.remainingQuestions],
        currentIncorrectPile: [],
        aggregateIncorrectPile: [...quizItem.aggregateIncorrectPile, ...quizItem.currentIncorrectPile]
    };
};



const markIncorrect = (quizItem: IQuizState, atomicNumber: number): IQuizState => {
    return {
        ...quizItem,
        currentIncorrectPile: [...quizItem.currentIncorrectPile, atomicNumber]
    };
}