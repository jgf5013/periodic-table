import { withAuthenticator } from '@aws-amplify/ui-react';
import { AppContextProvider } from './app-context';
import './App.scss';
import NavBar from './components/nav-bar/NavBar';
import PeriodicTable from './components/periodic-table/PeriodicTable';
import QuizItem from './components/quiz-item/QuizItem';

function App() {
  return (
    <div className="App">
      <NavBar />
      <div className="App-container">
        <AppContextProvider>
          <div className="main-content">
            <QuizItem/>
            <PeriodicTable />
          </div>
        </AppContextProvider>
      </div>
    </div>
  );
}

export default withAuthenticator(App);

