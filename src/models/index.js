// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Element } = initSchema(schema);

export {
  Element
};