/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createElement = /* GraphQL */ `
  mutation CreateElement(
    $input: CreateElementInput!
    $condition: ModelElementConditionInput
  ) {
    createElement(input: $input, condition: $condition) {
      id
      name
      appearance
      atomicMass
      boil
      category
      color
      density
      discoveredBy
      melt
      molarHeat
      namedBy
      number
      period
      phase
      source
      spectralImg
      summary
      symbol
      xpos
      ypos
      shells
      electronConfiguration
      electronConfigurationSemantic
      electronAffinity
      electroNegativityPauling
      ionizationEnergies
      cpkHex
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const updateElement = /* GraphQL */ `
  mutation UpdateElement(
    $input: UpdateElementInput!
    $condition: ModelElementConditionInput
  ) {
    updateElement(input: $input, condition: $condition) {
      id
      name
      appearance
      atomicMass
      boil
      category
      color
      density
      discoveredBy
      melt
      molarHeat
      namedBy
      number
      period
      phase
      source
      spectralImg
      summary
      symbol
      xpos
      ypos
      shells
      electronConfiguration
      electronConfigurationSemantic
      electronAffinity
      electroNegativityPauling
      ionizationEnergies
      cpkHex
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;

export const deleteElement = /* GraphQL */ `
  mutation DeleteElement(
    $input: DeleteElementInput!
    $condition: ModelElementConditionInput
  ) {
    deleteElement(input: $input, condition: $condition) {
      id
      _version
    }
  }
`;
