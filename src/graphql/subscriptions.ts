/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateElement = /* GraphQL */ `
  subscription OnCreateElement {
    onCreateElement {
      id
      name
      appearance
      atomicMass
      boil
      category
      color
      density
      discoveredBy
      melt
      molarHeat
      namedBy
      number
      period
      phase
      source
      spectralImg
      summary
      symbol
      xpos
      ypos
      shells
      electronConfiguration
      electronConfigurationSemantic
      electronAffinity
      electroNegativityPauling
      ionizationEnergies
      cpkHex
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateElement = /* GraphQL */ `
  subscription OnUpdateElement {
    onUpdateElement {
      id
      name
      appearance
      atomicMass
      boil
      category
      color
      density
      discoveredBy
      melt
      molarHeat
      namedBy
      number
      period
      phase
      source
      spectralImg
      summary
      symbol
      xpos
      ypos
      shells
      electronConfiguration
      electronConfigurationSemantic
      electronAffinity
      electroNegativityPauling
      ionizationEnergies
      cpkHex
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteElement = /* GraphQL */ `
  subscription OnDeleteElement {
    onDeleteElement {
      id
      name
      appearance
      atomicMass
      boil
      category
      color
      density
      discoveredBy
      melt
      molarHeat
      namedBy
      number
      period
      phase
      source
      spectralImg
      summary
      symbol
      xpos
      ypos
      shells
      electronConfiguration
      electronConfigurationSemantic
      electronAffinity
      electroNegativityPauling
      ionizationEnergies
      cpkHex
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
