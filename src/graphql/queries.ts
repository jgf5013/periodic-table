/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const syncElements = /* GraphQL */ `
  query SyncElements(
    $filter: ModelElementFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncElements(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        name
        appearance
        atomicMass
        boil
        category
        color
        density
        discoveredBy
        melt
        molarHeat
        namedBy
        number
        period
        phase
        source
        spectralImg
        summary
        symbol
        xpos
        ypos
        shells
        electronConfiguration
        electronConfigurationSemantic
        electronAffinity
        electroNegativityPauling
        ionizationEnergies
        cpkHex
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
export const getElement = /* GraphQL */ `
  query GetElement($id: ID!) {
    getElement(id: $id) {
      id
      name
      appearance
      atomicMass
      boil
      category
      color
      density
      discoveredBy
      melt
      molarHeat
      namedBy
      number
      period
      phase
      source
      spectralImg
      summary
      symbol
      xpos
      ypos
      shells
      electronConfiguration
      electronConfigurationSemantic
      electronAffinity
      electroNegativityPauling
      ionizationEnergies
      cpkHex
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const listElements = /* GraphQL */ `
  query ListElements(
    $filter: ModelElementFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listElements(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        appearance
        atomicMass
        boil
        category
        color
        density
        discoveredBy
        melt
        molarHeat
        namedBy
        number
        period
        phase
        source
        spectralImg
        summary
        symbol
        xpos
        ypos
        shells
        electronConfiguration
        electronConfigurationSemantic
        electronAffinity
        electroNegativityPauling
        ionizationEnergies
        cpkHex
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
